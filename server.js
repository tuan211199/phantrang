var express = require('express');
var server = express();
var path = require('path');
var data = require('./data');

var cors = require('cors');
const amount = 10;

server.use(cors());

server.get('/api/v1/:page', function(req, res, next){
    var page = parseInt(req.params.page) -1 ;
    page = page < 0 ? 0 : page ;
    var start = page * amount;
    var end = start + amount;
    res.json({
        status: 'success',
        data : data.slice(start, end)
    });
})

server.get('/', function(req, res, next){
    res.sendFile(path.join(__dirname, 'index.html'))
})

server.listen(process.env.PORT || 3000, function(){
    console.log('Server is listening on port %d in %s mode !', this.address().port, server.settings.env);
})
